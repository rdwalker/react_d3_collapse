import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import * as d3 from 'd3';
import logo from './logo.svg';
import './App.css';


// Generate some hard-coded data to use for d3
// Nodes will collapse in the source -> target direction recursively
const genTree = () => {
    return {
        nodes: [
            {id: 0, collapsing: 0, collapsed: false},
            {id: 1, collapsing: 0, collapsed: false},
            {id: 2, collapsing: 0, collapsed: false},
            {id: 3, collapsing: 0, collapsed: false},
            {id: 4, collapsing: 0, collapsed: false},
            {id: 5, collapsing: 0, collapsed: false},
            {id: 6, collapsing: 0, collapsed: false},
            {id: 7, collapsing: 0, collapsed: false},
            {id: 8, collapsing: 0, collapsed: false},
            ],
        links: [
            {id: 1, source: 0, target: 1},
            {id: 2, source: 0, target: 2},
            {id: 3, source: 0, target: 3},
            {id: 4, source: 1, target: 4},
            {id: 5, source: 1, target: 5},
            {id: 6, source: 4, target: 5},
            {id: 7, source: 7, target: 8},
            {id: 8, source: 8, target: 5},
            {id: 9, source: 2, target: 6},
        ]
    };
};

// Setup scale
d3.scaleOrdinal(d3.schemeCategory10);

// Class that controls our d3 force graph
class force {
    constructor() {
        this.width = 300;
        this.height = 600;

        this.initForce();
    }

    // Setup the force, but don't attach any nodes or links yet.
    initForce = () => {
        this.force = d3.forceSimulation()
            .force("charge", d3.forceManyBody())
            .force("link", d3.forceLink().distance(25).strength(0.2))
            .force("center", d3.forceCenter(this.width / 2, this.height / 2))
            .force("collide", d3.forceCollide([5]).iterations([5]))
            .alphaDecay(0.02)
            .velocityDecay(0.1)
    };

    updateNode = (node) => {
        node
            .attr("transform", (d) => "translate(" + d.x + "," + d.y + ")")
    };

    updateLink = (link) => {
        link
            .attr('d', d => `M ${d.source.x}, ${d.source.y} L ${d.target.x}, ${d.target.y}`)
    };


    updateGraph = (graph) => {
        graph.selectAll('.node')
            .call(this.updateNode);
        graph.selectAll('.link')
            .call(this.updateLink);
    };

    dragStarted = (d) => {
        if (!d3.event.active) this.force.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y
    };

    dragging = (d) => {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    };

    dragEnded = (d) => {
        if (!d3.event.active) this.force.alphaTarget(0);
        d.fx = null;
        d.fy = null
    };

    drag = (node) => d3.select(node)
        .call(d3.drag()
            .on("start", this.dragStarted)
            .on("drag", this.dragging)
            .on("end", this.dragEnded)
        );

    tick = (that) => {
        console.log('tick');
        const canvas = d3.select(that);
        this.force.on('tick', () => {
            canvas.call(this.updateGraph)
        });
    };

    zoom = (that) => {
        const canvas = d3.select(that);
        canvas.call(
            d3.zoom()
                .scaleExtent([0.00001, 10000000])
                .on("zoom", this.zoomed)
        )
        .on("dblclick.zoom", null);
    };

    zoomed = () => {
        d3.selectAll('g[id="links"]').attr('transform', d3.event.transform);
        d3.selectAll('g[id="nodes"]').attr('transform', d3.event.transform);
    };
}

class App extends Component {
    constructor(props) {
        super(props);
        // Get the data
        const {nodes, links} = genTree();
        // Assign to state for tracking changes and refreshing d3
        this.state = {
            nodes: nodes,
            links: links,
        };
        // Use the force Luke
        this.FORCE = new force();
        // Give d3 some nodes and links to apply the force to
        this.FORCE.force.nodes(nodes);
        this.FORCE.force.force("link").links(links);

        // Bind local methods
        this.addNode = this.addNode.bind(this);
        this.collapseNode = this.collapseNode.bind(this);

        // SVG ref for d3 zoom/scale
        this.svgRef = React.createRef();
    }

    componentDidMount() {
        // give d3 a tick to show it's alive! but mostly to update the node/link SVG paths
        this.FORCE.tick(this.svgRef.current);
        // Let there be zoom!
        this.FORCE.zoom(this.svgRef.current);
    }

    componentDidUpdate(prevProps, prevState) {
        // After we update nodes (because something was clicked and now we have to collapse/expand)
        if (prevState.nodes !== this.state.nodes || prevState.links !== this.state.links) {
            let { nodes, links } = this.state;

            // Filter out the collapsed nodes/associated links
            nodes = nodes.filter(n => n.collapsing === 0);
            links = links.filter(l => l.source.collapsing === 0 && l.target.collapsing === 0);

            // update d3 with the un-collapsed nodes/links
            this.FORCE.force.nodes(nodes);
            this.FORCE.force.force("link").links(links);
        }
    }

    addNode(nodeData) {
        this.setState(prevState => ({
            nodes: [...prevState.nodes, {id: prevState.nodes.length,}],
            links: [...prevState.links, {
                id: prevState.links.length,
                source: nodeData.id,
                target: prevState.nodes.length,
                numParticles: 1,
            }]
        }));
    }

    collapseNode(nodeData) {
            const updatedNodes = [];
            let inc = nodeData.collapsed ? -1 : 1;
            const { links } = this.state;

            // Recursively loop through targets until we have no targets and collapse them
            const recurse = (nodeData) => {
                // Loop through all of the links in our network
                // If any of the sources match the node we clicked
                // then set the target to be collapsed and start checking the target's targets for additional
                // nodes we should also collapse
                // Stop when we run out of targets
                //
                // NOTE: This will break if you make an infinite loop of targets. e.g., A -> B -> C -> A
                links.forEach(l => {
                    if (l.source.id === nodeData.id) {
                        l.target.collapsing += inc;
                        recurse(l.target);
                    }
                });
                nodeData.collapsed = !nodeData.collapsed;
                updatedNodes.push(nodeData);
            };
            recurse(nodeData);
            this.updateNodes(updatedNodes);
    }

    // Update the state with the nodes that have been collapsed/expanded
    updateNodes(updatedNodes) {
        let { nodes, links } = this.state;
        updatedNodes.map(un => {
            const prevNode = nodes.find(n => n.id === un.id);
            const index = nodes.indexOf(prevNode);
            return nodes.splice(index, 1, un);
        });
        this.setState({
            nodes,
            links
        });
    }

    render() {

        const links = this.state.links.map((link) => {
            // Don't show the links that are collapsed
            if (link.source.collapsing === 0 && link.target.collapsing === 0) {
                return (
                    <Link
                        key={link.id}
                        data={link}
                        force={this.FORCE}
                    />);
            }
            return null;
        });

        const nodes = this.state.nodes.map((node) => {
            // Don't show the nodes that are collapsed
            if (node.collapsing === 0) {
                return (
                    <Node
                        data={node}
                        key={node.id}
                        collapseNode={this.collapseNode}
                        force={this.FORCE}
                    />);
            }
            return null;
        });

        const width = window.innerWidth;
        const height = window.innerHeight;

        return (
            <div>
                <p>Click on a node to collapse its targets</p>
                <div className={'chart'}>
                    <svg
                        width={'100%'}
                        height={'100%'}
                        viewBox={`0,0,${Math.min(width, height)},${Math.min(width, height)}`}
                        preserveAspectRatio={'xMidYMid meet'}
                        ref={this.svgRef}
                    >
                        <g id={'links'}>
                            {links}
                        </g>
                        <g id={'nodes'}>
                            {nodes}
                        </g>
                    </svg>
                </div>
            </div>
        );
    }
}

class Link extends Component {

    componentDidMount() {
        this.d3Link = d3.select(ReactDOM.findDOMNode(this))
            .datum(this.props.data)
    }

    componentDidUpdate() {
        this.d3Link.datum(this.props.data)
            .call(this.props.force.updateLink);
    }

    render() {
        return (
            <path
                className='link'
                stroke={'gray'}
                strokeWidth={1}
                opacity={0.8}
                onMouseOver={() => console.log('mouse over link ' + this.props.data.id)}
            />
        );
    }
}

class Node extends Component {

    componentDidMount() {
        this.d3Node = d3.select(this.nodeRef.current)
            .datum(this.props.data);
        this.props.force.drag(this.nodeRef.current);
    }

    componentDidUpdate() {
        this.d3Node.datum(this.props.data)
            .call(this.props.force.updateNode)
    }

    nodeRef = React.createRef();

    render() {
        return (
            <g
                className='node'
                transform={`translate(${this.props.data.x || 0},${this.props.data.y || 0})`}
                onClick={() => this.props.collapseNode(this.props.data)}
                onDoubleClick={() => console.log('Double Clicked')}
                onMouseOver={() => console.log('mouse over node ' + this.props.data.id)}
                ref={this.nodeRef}
            >
                <image
                    xlinkHref={logo}
                    width={50}
                    height={50}
                    x={-25}
                    y={-25}
                >
                </image>
                <text
                    textAnchor={'middle'}
                    alignmentBaseline={'middle'}
                    fontSize={10}
                >
                    {this.props.data.id}
                </text>
            </g>
        );
    }
}

export default App;